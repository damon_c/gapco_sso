from rest_framework_simplejwt import views
from gapco_sso import serializers


class TokenObtainPairView(views.TokenObtainPairView):
    serializer_class = serializers.TokenObtainPairSerializer

class TokenRefreshView(views.TokenRefreshView):
    serializer_class = serializers.TokenRefreshSerializer

class TokenVerifyView(views.TokenVerifyView):
    pass
