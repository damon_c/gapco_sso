from django.utils.functional import cached_property
from rest_framework_simplejwt.models import TokenUser


class GAATokenUser(TokenUser):

    @cached_property
    def username(self):
        return self.token.get('name', '')

    @cached_property
    def name(self):
        return self.token.get('name', '')

    @cached_property
    def uuid(self):
        return self.id

    @cached_property
    def email(self):
        return self.token.get('email')

