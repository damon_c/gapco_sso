import os
from datetime import timedelta

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(days=1),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=10),
    'AUTH_HEADER_TYPES': ('JWT',),
    'USER_ID_FIELD': 'uuid',
    'USER_ID_CLAIM': 'uuid',
    'SIGNING_KEY': os.getenv('AUTH_SIGN_KEY') or '12345',
    'AUTH_TOKEN_CLASSES': ('gapco_sso.token.GAAAuthToken',),
    'TOKEN_USER_CLASS': 'gapco_sso.user.GAATokenUser',
}
