from django.contrib.auth import login

from rest_framework_simplejwt import serializers
from gapco_sso.token import GAARefreshToken
from rest_framework_simplejwt.settings import api_settings

class TokenObtainPairSerializer(serializers.TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        return GAARefreshToken.for_user(user)
    
    def validate_email(self, value):
        if value:
            return value.lower()
    
    def validate(self, attrs):
        result = super().validate(attrs)
        login(self.context['request'], self.user)
        return result


class TokenRefreshSerializer(serializers.TokenRefreshSerializer):

    def validate(self, attrs):
        refresh = GAARefreshToken(attrs['refresh'])

        data = {'access': str(refresh.access_token)}

        if api_settings.ROTATE_REFRESH_TOKENS:
            if api_settings.BLACKLIST_AFTER_ROTATION:
                try:
                    # Attempt to blacklist the given refresh token
                    refresh.blacklist()
                except AttributeError:
                    # If blacklist app not installed, `blacklist` method will
                    # not be present
                    pass

            refresh.set_jti()
            refresh.set_exp()

            data['refresh'] = str(refresh)

        return data
