from rest_framework_simplejwt.tokens import AccessToken, RefreshToken


class GAAUserTokenMixin:

    @classmethod
    def decorate_token(cls, user, token):
        token['is_staff'] = user.is_staff
        token['name'] = user.name
        token['email'] = user.email

        return token


class GAARefreshToken(GAAUserTokenMixin, RefreshToken):

    @classmethod
    def for_user(cls, user):
        token = RefreshToken.for_user(user)
        return cls.decorate_token(user, token)

    @property
    def access_token(self):
        """
        Returns an access token created from this refresh token.  Copies all
        claims present in this refresh token to the new access token except
        those claims listed in the `no_copy_claims` attribute.
        """
        access = GAAAuthToken()

        # Use instantiation time of refresh token as relative timestamp for
        # access token "exp" claim.  This ensures that both a refresh and
        # access token expire relative to the same time if they are created as
        # a pair.
        access.set_exp(from_time=self.current_time)

        no_copy = self.no_copy_claims
        for claim, value in self.payload.items():
            if claim in no_copy:
                continue
            access[claim] = value

        return access


class GAAAuthToken(GAAUserTokenMixin, AccessToken):

    @classmethod
    def for_user(cls, user):
        token = AccessToken.for_user(user)
        return cls.decorate_token(user, token)


