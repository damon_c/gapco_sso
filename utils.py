import datetime
import base64
import hashlib
import json
from cryptography.fernet import Fernet
from django.core import signing
from django.conf import settings
from django.contrib.auth import get_user_model
from gapco_sso.token import GAARefreshToken

from rest_framework import authentication


def _generate_key():
    m = hashlib.sha256()
    m.update(settings.AUTH_SIGN_KEY.encode('utf-8'))
    key = m.digest()
    return base64.urlsafe_b64encode(key)


def _encrypt(data):

    f = Fernet(_generate_key())
    return f.encrypt(data)


def _decrypt(data):
    f = Fernet(_generate_key())
    return f.decrypt(data)


def decode_signed_data(data):

    decoded = _decrypt(data.encode()).decode('ascii')
    return json.loads(base64.b64decode(decoded))


def encode_user(user_object):

    value = {
        'email': user_object.email,
        'name': user_object.name,
        'is_staff': user_object.is_staff,
        'password': user_object.password,  # this is hashed
        'uuid': str(user_object.uuid),
        'time': datetime.datetime.utcnow().isoformat(),
    }

    return encode_serialized_user(value)


def encode_login_data(user_object, token):

    value = {
        'email': user_object.email,
        'token': token,
    }

    return encode_serialized_user(value)


def encode_serialized_user(data):
    value = base64.b64encode(bytes(json.dumps(data).encode('ascii')))
    return _encrypt(value).decode('ascii')


def get_encoded_user(data):
    return get_user_model().objects.get(uuid=decode_signed_data(data)['uuid'])


class SSOAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        try:
            return (
                get_encoded_user(
                    request.META.get('HTTP_GAPCOAUTHCODE', request.GET.get('encoded'))),
                None
            )
        except Exception:
            return None


def get_admin_internal_request_headers(user_instance=None):
    user = user_instance.user if hasattr(user_instance, 'user') else user_instance

    return {
        "content-type": 'application/json',
        "accept": 'application/json',
        "Authorization": "JWT " + str(GAARefreshToken().for_user(user).access_token)
    }
